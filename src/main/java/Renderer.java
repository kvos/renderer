import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.lwjgl.BufferUtils;
import org.lwjgl.system.MemoryStack;

import java.nio.FloatBuffer;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.lwjgl.opengl.GL11.GL_TRUE;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.*;
import static org.lwjgl.system.MemoryUtil.memFree;

public class Renderer {
    float ratio;

    int vertexShader;
    int fragmentShader;
    int shaderProgram;

    int vao;
    int vbo;

    int positionAttrib;
    int colorAttrib;

    int modelUniform;
    int viewUniform;
    int projectionUniform;

    FloatBuffer modelBuffer;
    FloatBuffer viewBuffer;
    FloatBuffer projectionBuffer;

    public void init(float ratio) {
        this.ratio = ratio;

        // Load and compile shaders

        vertexShader = glCreateShader(GL_VERTEX_SHADER);
        glShaderSource(vertexShader, readFile("src/main/resources/vertex.glsl"));
        glCompileShader(vertexShader);

        int vertexShaderStatus = glGetShaderi(vertexShader, GL_COMPILE_STATUS);
        if (vertexShaderStatus != GL_TRUE) {
            throw new RuntimeException(glGetShaderInfoLog(vertexShader));
        }

        fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
        glShaderSource(fragmentShader, readFile("src/main/resources/fragment.glsl"));
        glCompileShader(fragmentShader);

        int fragmentShaderStatus = glGetShaderi(fragmentShader, GL_COMPILE_STATUS);
        if (fragmentShaderStatus != GL_TRUE) {
            throw new RuntimeException(glGetShaderInfoLog(fragmentShader));
        }

        // Link and use shader program

        shaderProgram = glCreateProgram();
        glAttachShader(shaderProgram, vertexShader);
        glAttachShader(shaderProgram, fragmentShader);
        glBindFragDataLocation(shaderProgram, 0, "fragColor");
        glLinkProgram(shaderProgram);

        int shaderProgramStatus = glGetProgrami(shaderProgram, GL_LINK_STATUS);
        if (shaderProgramStatus != GL_TRUE) {
            throw new RuntimeException(glGetProgramInfoLog(shaderProgram));
        }

        glUseProgram(shaderProgram);

        vao = glGenVertexArrays();
        glBindVertexArray(vao);

        try (MemoryStack stack = MemoryStack.stackPush()) {
            FloatBuffer vertices = stack.mallocFloat(3 * 6);
            vertices.put(-0.6f).put(-0.4f).put(0f).put(1f).put(0f).put(0f);
            vertices.put(0.6f).put(-0.4f).put(0f).put(0f).put(1f).put(0f);
            vertices.put(0f).put(0.6f).put(0f).put(0f).put(0f).put(1f);
            vertices.flip();

            vbo = glGenBuffers();
            glBindBuffer(GL_ARRAY_BUFFER, vbo);
            glBufferData(GL_ARRAY_BUFFER, vertices, GL_STATIC_DRAW);
        }

        positionAttrib = glGetAttribLocation(shaderProgram, "position");
        glEnableVertexAttribArray(positionAttrib);
        glVertexAttribPointer(positionAttrib, 3, GL_FLOAT, false, 6 * 4, 0);

        colorAttrib = glGetAttribLocation(shaderProgram, "color");
        glEnableVertexAttribArray(colorAttrib);
        glVertexAttribPointer(colorAttrib, 3, GL_FLOAT, false, 6 * 4, 3 * 4);

        modelUniform = glGetUniformLocation(shaderProgram, "model");
        modelBuffer = BufferUtils.createFloatBuffer(16);
        new Matrix4f().get(modelBuffer);
        glUniformMatrix4fv(modelUniform, false, modelBuffer);

        viewUniform = glGetUniformLocation(shaderProgram, "view");
        viewBuffer = BufferUtils.createFloatBuffer(16);
        new Matrix4f().get(viewBuffer);
        glUniformMatrix4fv(viewUniform, false, viewBuffer);

        projectionUniform = glGetUniformLocation(shaderProgram, "projection");
        projectionBuffer = BufferUtils.createFloatBuffer(16);
        new Matrix4f()
                .perspective((float) Math.toRadians(45f), ratio, 0f, 1f)
                .lookAt(0f, 0f, -2f,
                        0f, 0f, 0f,
                        0f, 1f, 0f)
                .get(projectionBuffer);
        glUniformMatrix4fv(projectionUniform, false, projectionBuffer);

        glEnable(GL_MULTISAMPLE);
    }

    public void destroy() {
        glDeleteShader(vertexShader);
        glDeleteShader(fragmentShader);
        glDeleteProgram(shaderProgram);

        memFree(modelBuffer);
        memFree(viewBuffer);
        memFree(projectionBuffer);

        glDeleteVertexArrays(vao);
    }

    public void render(float alpha, GameState gameState, GameState lastGameState) {
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        Vector3f lerpCameraPos = new Vector3f(lastGameState.getCameraPos())
                .lerp(new Vector3f(gameState.getCameraPos()), alpha);
        Vector3f lerpCameraDir = new Vector3f(lastGameState.getCameraDir())
                .lerp(new Vector3f(gameState.getCameraDir()), alpha);


        Vector3f center = new Vector3f(lerpCameraPos);
        center.add(lerpCameraDir);

        Vector3f up = new Vector3f(0f, 1f, 0f);

        new Matrix4f()
                .perspective((float) Math.toRadians(45f), ratio, 0f, 1f)
                .lookAt(lerpCameraPos, center, up)
                .get(projectionBuffer);
        glUniformMatrix4fv(projectionUniform, false, projectionBuffer);

        //float lerpAngle = (1f - alpha) * lastAngle + alpha * currentAngle;
        new Matrix4f()
                //.rotate(lerpAngle, 0f, 1f, 0f)
                .get(modelBuffer);
        glUniformMatrix4fv(modelUniform, false, modelBuffer);

        glDrawArrays(GL_TRIANGLES, 0, 3);
    }

    private static String readFile(final String path) {
        String fileContent;
        try {
            fileContent = Files.readString(Path.of(path));
        } catch (Exception e) {
            throw new RuntimeException("Failed to read " + path);
        }
        return fileContent;
    }
}
