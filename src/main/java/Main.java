import org.joml.Matrix3f;
import org.joml.Quaternionf;
import org.joml.Vector2d;
import org.joml.Vector3f;
import org.lwjgl.Version;
import org.lwjgl.glfw.Callbacks;
import org.lwjgl.glfw.GLFWCursorPosCallback;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWKeyCallback;
import org.lwjgl.opengl.GL;

import static org.lwjgl.glfw.GLFW.*;

public class Main {

    static int winWidth = 1920;
    static int winHeight = 1080;

    static Renderer renderer = new Renderer();

    // input data

    final static int moveLeftKey = GLFW_KEY_A;
    final static int moveRightKey = GLFW_KEY_D;
    final static int moveFrontKey = GLFW_KEY_W;
    final static int moveBackKey = GLFW_KEY_S;

    static int[] lastAction = new int[GLFW_KEY_LAST];

    static boolean moveLeft = false;
    static boolean moveRight = false;
    static boolean moveFront = false;
    static boolean moveBack = false;

    static Vector2d cursorPos = new Vector2d();

    // game state

    static GameState gameState;

    public static void main(String[] args) {

        // Setup window and rendering context

        System.out.println("LWJGL Version " + Version.getVersion());

        GLFWErrorCallback errorCallback = GLFWErrorCallback.createPrint(System.err);
        glfwSetErrorCallback(errorCallback);

        if (!glfwInit()) {
            throw new RuntimeException("Failed to initialize GLFW");
        }

        glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE); // Needed for floating window in i3wm

        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
        glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE);

        glfwWindowHint(GLFW_SAMPLES, 8);

        long winHandle = glfwCreateWindow(winWidth, winHeight, "Phy", 0, 0);
        if (winHandle == 0) {
            throw new RuntimeException("Failed to create a GLFW window");
        }

        glfwMakeContextCurrent(winHandle);
        GL.createCapabilities();
        glfwSwapInterval(0);

        // Setup input state and event handling

        GLFWKeyCallback keyCallback = new GLFWKeyCallback() {
            @Override
            public void invoke(long window, int key, int scancode, int action, int mods) {
                if (action == GLFW_PRESS || action == GLFW_RELEASE) {
                    if (key == moveLeftKey) {
                        if (action == GLFW_PRESS) {
                            moveLeft = true;
                            moveRight = false;
                        }
                        if (action == GLFW_RELEASE) {
                            moveLeft = false;
                            if (lastAction[moveRightKey] == GLFW_PRESS) {
                                moveRight = true;
                            }
                        }
                    }
                    if (key == moveRightKey) {
                        if (action == GLFW_PRESS) {
                            moveRight = true;
                            moveLeft = false;
                        }
                        if (action == GLFW_RELEASE) {
                            moveRight = false;
                            if (lastAction[moveLeftKey] == GLFW_PRESS) {
                                moveLeft = true;
                            }
                        }
                    }

                    if (key == moveFrontKey) {
                        if (action == GLFW_PRESS) {
                            moveFront = true;
                            moveBack = false;
                        }
                        if (action == GLFW_RELEASE) {
                            moveFront = false;
                            if (lastAction[moveBackKey] == GLFW_PRESS) {
                                moveBack = true;
                            }
                        }
                    }
                    if (key == moveBackKey) {
                        if (action == GLFW_PRESS) {
                            moveBack = true;
                            moveFront = false;
                        }
                        if (action == GLFW_RELEASE) {
                            moveBack = false;
                            if (lastAction[moveFrontKey] == GLFW_PRESS) {
                                moveFront = true;
                            }
                        }
                    }

                    if (key == GLFW_KEY_ESCAPE) {
                        glfwSetWindowShouldClose(winHandle, true);
                    }

                    lastAction[key] = action;
                }
            }
        };

        GLFWCursorPosCallback cursorPosCallback = new GLFWCursorPosCallback() {
            @Override
            public void invoke(long window, double xpos, double ypos) {
                cursorPos.set(xpos, ypos);
            }
        };

        glfwSetKeyCallback(winHandle, keyCallback);

        glfwSetInputMode(winHandle, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
        if (glfwRawMouseMotionSupported()) {
            glfwSetInputMode(winHandle, GLFW_RAW_MOUSE_MOTION, GLFW_TRUE);
        }
        glfwSetCursorPosCallback(winHandle, cursorPosCallback);

        // Set up fixed time step loop

        final float targetUps = 60f;
        final float tickTime = 1f / targetUps;

        float lastLoopStartTime = (float) glfwGetTime();
        float delta;
        float accumulator = 0f;
        float alpha;

        renderer.init(1f * winWidth / winHeight);

        // init game state
        gameState = new GameState();
        gameState.setCameraPos(new Vector3f(0f, 0f, 2f));
        gameState.setCameraDir(new Vector3f(0f, 0f, -1f));
        gameState.setTrianglePos(new Vector3f(0f, 0f, 0f));
        gameState.setTriangleDir(new Vector3f(0f, 0f, 1f));

        GameState lastGameState = new GameState();

        while (!glfwWindowShouldClose(winHandle)) {
                float loopStartTime = (float) glfwGetTime();
            delta = loopStartTime - lastLoopStartTime;
            lastLoopStartTime = loopStartTime;
            accumulator += delta;

            // setup temporary game state

            Vector2d lastCursorPos = new Vector2d();

            Vector3f cameraPos = new Vector3f(gameState.getCameraPos());
            Vector3f cameraDir = new Vector3f(gameState.getCameraDir());
            Vector3f trianglePos = new Vector3f(gameState.getTrianglePos());
            Vector3f triangleDir = new Vector3f(gameState.getTriangleDir());

            // simulate ticks until the accumulator is less than 1 tick
            while (accumulator >= tickTime) {
                // copy game state to temporary variables
                lastCursorPos.set(cursorPos);

                lastGameState.setCameraPos(cameraPos);
                lastGameState.setCameraDir(cameraDir);
                lastGameState.setTrianglePos(trianglePos);
                lastGameState.setTriangleDir(triangleDir);

                // get input

                glfwPollEvents();

                // update game state
                Vector3f up = new Vector3f(0f, 1f, 0f);
                Vector3f movement = new Vector3f(0f, 0f, 0f);
                Vector3f cameraFrontDir = new Vector3f(cameraDir)
                        .sub(0f, cameraDir.y, 0f)
                        .normalize();
                Vector3f cameraRightDir = new Vector3f(cameraDir).cross(up).normalize();

                Vector2d cursorPosDelta = new Vector2d(cursorPos).sub(lastCursorPos);
                float rotationUnitsPerTick = -0.002f;

                Matrix3f m = new Matrix3f().rotateY((float) cursorPosDelta.x * rotationUnitsPerTick)
                        .rotateLocal((float) cursorPosDelta.y * rotationUnitsPerTick,
                                cameraRightDir.x, cameraRightDir.y, cameraRightDir.z);
                cameraDir.mul(m);
                gameState.setCameraDir(cameraDir);

                if (moveFront) {
                    movement.add(cameraFrontDir);
                } else if (moveBack) {
                    movement.sub(cameraFrontDir);
                }

                if (moveRight) {
                    movement.add(cameraRightDir);
                } else if (moveLeft) {
                    movement.sub(cameraRightDir);
                }
                float movementUnitsPerTick = 0.02f;
                movement.mul(movementUnitsPerTick);

                cameraPos.add(movement);
                gameState.setCameraPos(cameraPos);

                triangleDir.rotateZ(1f);
                gameState.setTriangleDir(triangleDir);

                // Take off one tickTime from accumulator
                accumulator -= tickTime;
            }

            // the accumulator is now a fraction of 1 tickTime
            // in order to get the value of alpha at the start of rendering, we
            // add the current loop time to the accumulator before divding it
            // by tickTime. If we did not do this, we would get the alpha at the
            // start of simulating.
            float renderStartTime = (float) glfwGetTime();
            float currentLoopTime = renderStartTime - loopStartTime;
            alpha = (accumulator + currentLoopTime) / tickTime;

            renderer.render(alpha, gameState, lastGameState);

            glfwSwapBuffers(winHandle);
        }

        // Release resources

        renderer.destroy();

        glfwDestroyWindow(winHandle);
        Callbacks.glfwFreeCallbacks(winHandle);
        glfwTerminate();
        errorCallback.free();
    }
}
